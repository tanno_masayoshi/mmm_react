const defaultState = {
    user: [],
    isSignedIn: false,
    isLoginAnimation: false,
    termStart: "",
    termEnd: "",
    incomes: [],
    expenseArray: [],
    categoryArray: [],
    editIncomeId: 0,
    editExpenseId: 0,
    monthlyList: [],
    totalExpense: [],
    totalIncome: [],
}

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case 'LOGIN':
            return {
                ...state,
                user: action.user,
                isSignedIn: true
            };
        case 'LOGIN_ANIMATION':
            return {
                ...state,
                isLoginAnimation: true
            };
        case 'LOGOUT':
            return {
                ...state,
                user: [],
                isSignedIn: false,
                isLoginAnimation: false
            };
        case 'USER_EDIT':
            return {
                ...state,
                isUserEdit: action.isUserEdit
            };

        case 'UPDATE_LIST_TERM':
            return {
                ...state,
                termStart: action.termStart,
                termEnd: action.termEnd
            };

        case 'UPDATE_EXPENSE_LIST':
            return {
                ...state,
                expenseArray: action.expenseArray
            };

        case 'UPDATE_CATEGORY_LIST':
            return {
                ...state,
                categoryArray: action.categoryArray
            };

        case 'EDIT_EXPENSE':
            return {
                ...state,
                editExpenseId: action.editExpenseId
            };

        case 'GET_INCOME':
            return {
                ...state,
                incomes: action.incomes
            };

        case 'EDIT_INCOME':
            return {
                ...state,
                editIncomeId: action.editIncomeId
            };

        case 'GET_MONTHLY_LIST':
            return {
                ...state,
                monthlyList: action.monthlyList
            };

        case 'GET_TOTAL_EXPENSE':
            return {
                ...state,
                totalExpense: action.totalExpense
            };

        case 'GET_TOTAL_INCOME':
            return {
                ...state,
                totalIncome: action.totalIncome
            };

        default:
            return state;
    }
}
