import React, { Component } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import './css/calendar.css'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from './action/actions';
import PropTypes from 'prop-types';
import { withCookies } from 'react-cookie';
import { Button } from 'react-bootstrap';
import moment from 'moment';

class Plans extends Component {
  constructor(props) {
    super(props);
    const { dispatch } = props;
    this.action = bindActionCreators(actions, dispatch)
    this.state = {
      date: new Date(),
      //月のデータ
      month_days: {
        20210710: { memo: 'test' },
        20190517: { memo: 'test2' },
        20190518: {},
      },
      planMemo: ""
    };
    this.getTileClass = this.getTileClass.bind(this);
    this.getTileContent = this.getTileContent.bind(this);
  }

  // componentDidMount() {
  //   this.setState({
  //     ...this.state,
  //     month_days: {
  //       ...this.state,
  //       20190518: {
  //         text: "テスト"
  //       }
  //     }
  //   })
  // }

  // state の日付と同じ表記に変換
  getFormatDate(date) {
    return `${date.getFullYear()}${('0' + (date.getMonth() + 1)).slice(-2)}${('0' + date.getDate()).slice(-2)}`;
  }

  //日付のクラスを付与 (祝日用)
  getTileClass({ date, view }) {
    // 月表示のときのみ
    if (view !== 'month') {
      return '';
    }
    const day = this.getFormatDate(date);
    return (this.state.month_days[day] && this.state.month_days[day].is_holiday) ?
      'holiday' : '';
  }

  // 予定作成
  createPlan(day) {
    console.log(moment(day).format("yyyy/MM/DD"))
    return(
      <>
      <input type="textarea"/>
      </>
    )
  }
  handleChange(value){
    // const value = event.target.value
    this.setState({planMemo: value})
  }

  //日付の内容を出力
  getTileContent({ date, view }) {
    // 月表示のときのみ
    if (view !== 'month') {
      return null;
    }
    const day = this.getFormatDate(date);
    return (
      <p>
        <br />
        {(this.state.month_days[day] && this.state.month_days[day].memo) ?
          this.state.month_days[day].memo :
          <div>
          <input value={this.state.planMemo} onChange={() => this.handleChange(event.target.value)} type="text" style={{width: "100px"}}/>
          <Button onClick={() => this.createPlan(day)} className="addButton btn btn-light rounded-circle p-0 "
          style={{width: "20px",
          height:"20px",
          fontSize: "13px",
          color: "gray"}}>
            ＋
          </Button>
          </div>
        }
      </p>
    );
  }

  render() {
    return (
      <>
        <p className="calendar-title">収支メモ</p>
        <Calendar
          locale="ja-JP"
          value={this.state.date}
          tileClassName={this.getTileClass}
          tileContent={this.getTileContent}
        />
      </>
    );
  }
}

Plans.propTypes = {
  dispatch: PropTypes.func,
  user: PropTypes.any,
  isSignedIn: PropTypes.bool,
  cookies: PropTypes.any
}

function mapStateToProps(state) {
  return state
}

export default withCookies(connect(mapStateToProps)(Plans));