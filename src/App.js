import React, { Component } from 'react';
// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './action/actions'
import PropTypes from 'prop-types';
// Cookie
import { withCookies } from 'react-cookie';
// Style Sheet
import './css/app.css';
// Component
import InAndOut from './components/InAndOut';
import Starting from './components/users/starting';
import User from './components/User';

class App extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch)
    }

    componentDidMount() {
        // クッキーのログイン情報を参照して、ログインしていれば、stateを変更する(ログイン状態・ユーザー情報・収支データ)
        const { cookies } = this.props;
        if (cookies.get('isSignedIn')) {
            this.action.login(cookies.get('user'))
        }
    }

    // ログイン後の処理をここへ記述
    aplicationContents() {
        return (
            <div className="main-contents">
                <InAndOut />
            </div>
        )
    }

    render() {
        return (
            <div className="main-wrpper">
                <div className="header">
                    <div className="header-menu">
                        <img className="logo" src="https://fontmeme.com/permalink/210704/bebc5fc9b26dbee441b8e42ad88776d3.png" alt="back-to-black-font" border="0" />
                        {this.props.isSignedIn &&
                            <div className="accountMenu">
                                <User />
                            </div>
                        }
                    </div>
                </div>

                <div className="contents">
                    {this.props.isSignedIn ?
                        this.aplicationContents() :
                        <Starting />
                    }
                </div>
            </div>
        )
    }
}

App.propTypes = {
    isSignedIn: PropTypes.bool,
    dispatch: PropTypes.func,
    cookies: PropTypes.any,
    user: PropTypes.any,
    expenseArray: PropTypes.array,
    start: PropTypes.any,
    end: PropTypes.any
}

function mapStateToProps(state) {
    return state
}

export default withCookies(connect(mapStateToProps)(App));
