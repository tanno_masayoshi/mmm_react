export function login(user) {
    return {
        type: 'LOGIN',
        user: user,
        isSignedIn: true
    }
}

export function logAnimation() {
    return {
        type: 'LOGIN_ANIMATION'
    }
}

export function logout() {
    return {
        type: 'LOGOUT',
        user: [],
        isSignedIn: false,
    }
}

export function updateListTerm(start, end) {
    return {
        type: 'UPDATE_LIST_TERM',
        termStart: start,
        termEnd: end
    }
}

export function updateExpenseList(expenseArray) {
    return {
        type: 'UPDATE_EXPENSE_LIST',
        expenseArray: expenseArray
    }
}

export function updateCategoryList(categoryArray) {
    return {
        type: 'UPDATE_CATEGORY_LIST',
        categoryArray: categoryArray
    }
}

export function editExpense(editExpenseId) {
    return {
        type: 'EDIT_EXPENSE',
        editExpenseId: editExpenseId
    }
}

export function getIncomes(json) {
    return {
        type: 'GET_INCOME',
        incomes: json
    }
}

export function editIncome(editIncomeId) {
    return {
        type: 'EDIT_INCOME',
        editIncomeId: editIncomeId
    }
}

export function getMonthlyList(monthlyList) {
    return {
        type: 'GET_MONTHLY_LIST',
        monthlyList: monthlyList
    }
}
