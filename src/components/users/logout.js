import React, {Component} from 'react';
// redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../../action/actions'
import PropTypes from 'prop-types';
// クッキー
import { withCookies } from 'react-cookie';
// BootStrap インポート
import 'bootstrap/dist/css/bootstrap.min.css'
import { Button } from 'react-bootstrap';

class Logout extends Component {
    constructor(props) {
        super(props);
        const{dispatch} = props;
        this.action = bindActionCreators(actions, dispatch);
    }

    // ログアウト処理
    handleLogout() {
        this.action.logout();
        // クッキー情報変更
        const { cookies } = this.props;
        cookies.set('isSignedIn', '', { path: "/" });
        cookies.set('user', '[]', {path:"/"});
    }

    render() {
        return(
            <div className="logout-area">
                <Button onClick={() => this.handleLogout()}>ログアウト</Button>
            </div>
        )
    }
}

Logout.propTypes = {
    dispatch: PropTypes.func,
    user: PropTypes.any,
    isSignedIn: PropTypes.bool,
    cookies: PropTypes.any
}

function mapStateToProps(state) {
    return state;
}

export default withCookies(connect(mapStateToProps)(Logout));
