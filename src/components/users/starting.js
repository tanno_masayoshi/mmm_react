import React, { Component } from 'react';
// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../action/actions'
import PropTypes from 'prop-types';
// Cookie
import { withCookies } from 'react-cookie';
// BootStrap インポート
import 'bootstrap/dist/css/bootstrap.min.css'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
// Component
import Signup from './signup';
import Login from './login';
// CSS
import '../../css/starting.css';

class Starting extends Component {
    constructor(props) {
        super(props);
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch)
    }

    render() {
        return (
            <>
                <div className="user-main" id="userMain" style={{
                transition: '1s',
                opacity: this.props.isLoginAnimation ? 0 : 1
            }}>
                    <div className="login-or-signup user-main" style={{
                transition: '1s',
                top: this.props.isLoginAnimation ? "0%" : "20%"
            }}>
                        <Tabs>
                            <TabList >
                                <Tab>ログイン</Tab>
                                <Tab>新規登録</Tab>
                            </TabList>
                            <TabPanel>
                                <Login />
                            </TabPanel>
                            <TabPanel>
                                <Signup />
                            </TabPanel>
                        </Tabs>
                    </div>
                </div>
            </>
        )
    }

}

Starting.propTypes = {
    isSignedIn: PropTypes.bool,
    dispatch: PropTypes.func,
    cookies: PropTypes.any,
    useState: PropTypes.func,
    isLoginAnimation: PropTypes.bool
}

function mapStateToProps(state) {
    return state
}

export default withCookies(connect(mapStateToProps)(Starting));