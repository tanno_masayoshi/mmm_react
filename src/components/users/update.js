import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from '../../action/actions';
import PropTypes from 'prop-types';
import { withCookies } from 'react-cookie';
import moment from 'moment';
import '../../css/userUpdate.css';
import { Form, Button, Alert } from 'react-bootstrap';

class Update extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch)
        this.state = {
            users: [],
            account: props.user.account,
            accountLength: 0,
            name: props.user.name,
            nameLength: 0,
            password: '',
            passwordLength: 0,
            passwordConfirmation: '',
            referenceDate: moment(props.user.referenceDate).format('yyyy-MM-DD'),
            isValid: true,
            isEditSuccess: false,
            isAccountError: false,
            isNameError: false,
            isReferenceError: false,
            isPasswordError: false,
            isSpaceErr: false,
            accountErrMsg: "",
            nameErrMsg: "",
            referenceErrMsg: "",
            passwordErrMsg: "",
            confirmationErrMsg: ""
        }
    }

    componentDidMount() {
        this.setState({
            accountLength: this.state.account.length,
            nameLength: this.state.name.length
        })

        // ユーザー重複バリデーション用
        fetch("http://localhost:8080/users", {
            method: "GET"
        }).then((response) => {
            if (response.status === 200) {
                response.json().then(json => {
                    // アカウントをStateへ保存
                    this.setState({ users: json })
                })
            } else {
                alert("通信エラー")
            }
        })
            .catch(error => console.log(error));
    }

    handleAccountChange(event) {
        const value = event.target.value;
        this.setState({
            account: value,
            accountLength: value.length
        });
        // 文字数カウント
        const element = document.getElementById('accountLen');
        if (value.length <= 5 || value.length > 20) {
            element.style.color = "red";
        } else {
            element.style.color = "green";
        }
        // アカウント重複
        let accountMatch = false
        this.state.users.forEach(user => {
            if (value == user.account && this.props.user.id != user.id) {
                accountMatch = true;
                return;
            }
        });
        // バリデーション
        if (value.match("[^a-zA-Z0-9]")) {
            this.setState({
                isAccountError: true,
                accountErrMsg: "アカウントは半角英数字のみ使えます"
            })
        } else if (value.length < 6 || value.length > 20) {

            this.setState({
                isAccountError: true,
                accountErrMsg: ""
            })
        } else if (accountMatch) {
            this.setState({
                isAccountError: true,
                accountErrMsg: "アカウントが重複しています"
            })
        } else {
            this.setState({
                isAccountError: false,
                accountErrMsg: ""
            })
            const emelentStyle = document.getElementById('formBasicAccount').style;
            emelentStyle.border = "";
        }
    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({
            name: value,
            nameLength: value.length
        });
        const element = document.getElementById('nameLen');
        // 文字数カウント
        if (value.length <= 0 || value.length > 10) {
            element.style.color = "red";
        } else {
            element.style.color = "green";
        }
        // バリデーション
        if (value.match("[^ぁ-んァ-ヶ一-龠a-zA-Z 　ｂ-ｚ]")) {
            this.setState({
                isNameError: true,
                nameErrMsg: "名前に数字、記号は使えません"
            })
        } else {

            if (value.length <= 0 || value.length > 10) {
                this.setState({ isNameError: true })
            } else {
                this.setState({ isNameError: false })
                const emelentStyle = document.getElementById('formBasicName').style;
                emelentStyle.border = "";
            }

            this.setState({
                nameErrMsg: ""
            })
        }

    }
    handlePasswordChange(event) {
        const element = document.getElementById('passwordLen');
        const value = event.target.value;
        this.setState({
            password: value,
            passwordLength: value.length
        })
        // 値が空の場合は、確認フォームも空にする
        if (value.length == 0) {
            element.style.color = "gray"
            this.setState({
                isPasswordError: false,
                isConfirmationError: false,
                passwordConfirmation: ""
            })
        } else {
            // 文字数カウント
            let isCountError = true
            if (value.length < 6 || value.length > 20) {
                element.style.color = "red"
                isCountError = false
            } else {
                element.style.color = "green"
                isCountError = false
            }

            // スペース禁止
            let isSpaceError = false
            if (value.match('[ ]')) {
                isCountError = true;
                this.setState({
                    isSpaceErr: true,
                    passwordErrMsg: 'スペースは使用できません'
                })
            } else {
                isCountError = false;
                this.setState({
                    isSpaceErr: false,
                    passwordErrMsg: ''
                })
            }

            if (!isCountError && !isSpaceError) {
                this.setState({
                    isPasswordError: false,
                    passwordErrMsg: ""
                })
            } else {
                this.setState({
                    isPasswordError: true
                })
            }
        }
    }

    handlePasswordConfirmationChange(event) {
        const element = document.getElementById('passwordCheck');
        const value = event.target.value;
        this.setState({ passwordConfirmation: value })

        if (value == this.state.password) {
            element.style.color = "green";
            this.setState({
                confirmationErrMsg: "確認OK",
                isConfirmationError: false
            })
        } else {
            element.style.color = "red";
            this.setState({
                confirmationErrMsg: "パスワードが一致しません",
                isConfirmationError: true
            })
        }

    }
    handleReferenceDateChange(event) {
        const value = event.target.value;
        this.setState({ referenceDate: value });
    }

    // アカウント情報変更
    updateAccount() {
        this.setState({
            password: "",
            passwordLength: 0,
            passwordConfirmation: ""
        })
        if (this.state.isAccountError || this.state.isNameError || this.state.isReferenceError || this.state.isPasswordError || this.state.isConfirmationError || this.state.password != this.state.passwordConfirmation) {
            this.setState({ isValid: false })
            if (this.state.password != this.state.passwordConfirmation) {
                this.setState({
                    passwordErrMsg: "パスワードが一致しません",
                    isSpaceErr: true
                })
            }
        } else {
            const data = { id: this.props.user.id, account: this.state.account, name: this.state.name, password: this.state.password, referenceDate: this.state.referenceDate }
            fetch("http://localhost:8080/users/edit/" + this.props.user.id, {
                headers: {
                    'Content-Type': 'application/json'
                },
                method: "PUT",
                body: JSON.stringify(data)
            }).then((response) => {
                if (response.status === 200) {
                    response.json().then(json => {
                        this.action.login(json)
                        // クッキー更新
                        const { cookies } = this.props;
                        cookies.set('user', json, { path: "/" });
                        this.setState({
                            isValid: true,
                            isEditSuccess: true
                        })

                        setTimeout(() => {
                            this.setState({ isEditSuccess: false })
                        }, 1000);
                        this.props.submitSuccess()
                    })


                } else {
                    alert("失敗しました")
                }
            }).catch(error => console.log(error));
        }
    }

    successFunc() {
        return (
            <Alert variant="info text-center" className="edit-error-alert">
                変更完了
            </Alert>
        )
    }

    render() {
        return (
            <div className="user-edit-form">
                {this.state.isEditSuccess &&
                    <Alert variant="info text-center" className="edit-error-alert">
                        変更完了
                    </Alert>
                }
                {!this.state.isValid &&
                    <Alert variant="danger text-center" className="edit-error-alert">
                        登録に失敗しました
                    </Alert>
                }

                <Form className="input-regist-form">
                    <Form.Group className="form-object" controlId="formBasicAccount">
                        <Form.Label className="form-label">アカウント</Form.Label>
                        <div className="form-error">
                            <p className="err-msg">
                                {this.state.isAccountError && this.state.accountErrMsg}
                            </p>
                            <Form.Control className="" onChange={(event) => this.handleAccountChange(event)} value={this.state.account} type="text" placeholder="半角英数字のみ使えます" style={{ border: this.state.isAccountError && '2px solid red' }} />
                            <p className="length-check"><span className="account-length" id="accountLen">{this.state.accountLength}</span>/20</p>
                        </div>
                    </Form.Group>
                    <Form.Group className="form-object" controlId="formBasicName">
                        <Form.Label className="form-label">氏名</Form.Label>
                        <div className="form-error">

                            {this.state.isNameError && <p className="err-msg">{this.state.nameErrMsg}</p>}

                            <Form.Control className="" onChange={(event) => this.handleNameChange(event)} value={this.state.name} type="text" placeholder="記号は使えません" style={{ border: this.state.isNameError && '2px solid red' }} />
                            <p className="length-check mb-0"><span className="name-length" id="nameLen">{this.state.nameLength}</span>/10</p>
                        </div>
                    </Form.Group>
                    <Form.Group className="form-object" controlId="formBasicReferenceDate">
                        <Form.Label className="form-label">基準日</Form.Label>
                        <div className="form-error">
                            <p className="err-msg">
                                {this.state.isReferenceError && this.state.referenceErrMsg}
                            </p>
                            <Form.Control className="" onChange={(event) => this.handleReferenceDateChange(event)} value={this.state.referenceDate} type="Date" style={{ border: this.state.isReferenceError && '2px solid red' }} />
                        </div>
                    </Form.Group>
                    <Form.Group className="form-object" controlId="formBasicPassword">
                        <Form.Label className="form-label">パスワード</Form.Label>
                        <div className="form-error">
                            <p className="err-msg">
                                {this.state.isSpaceErr && this.state.passwordErrMsg}
                            </p>
                            <Form.Control className="" onChange={(event) => this.handlePasswordChange(event)} value={this.state.password} type="password" placeholder="半角英数字と記号が使えます" style={{ border: this.state.isPasswordError && '2px solid red' }} />
                            <p className="length-check"><span className="password-length" id="passwordLen">{this.state.passwordLength}</span>/20</p>
                        </div>
                    </Form.Group>
                    {this.state.password.length > 5 && this.state.password.length < 21 && !this.state.isPasswordError &&
                        <Form.Group className="form-object" controlId="formBasicPasswordConfirmation">
                            <Form.Label className="form-label">パスワード(確認用)</Form.Label>
                            <div className="form-error">
                                <Form.Control className="" onChange={(event) => this.handlePasswordConfirmationChange(event)} value={this.state.passwordConfirmation} type="password" style={{ border: this.state.isConfirmationError && '2px solid red' }} />
                                <p className="length-check" id="passwordCheck">{this.state.confirmationErrMsg}</p>
                            </div>
                        </Form.Group>
                    }
                    <Button className="signupButton my-3" onClick={() => this.updateAccount()} variant="primary">
                        更新
                    </Button>
                </Form>
            </div>
        )
    }
}

Update.propTypes = {
    dispatch: PropTypes.func,
    cookies: PropTypes.any,
    user: PropTypes.any,
    submitSuccess: PropTypes.func
}

function mapStateToProps(state) {
    return state
}

export default withCookies(connect(mapStateToProps)(Update));