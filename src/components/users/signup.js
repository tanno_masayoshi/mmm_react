import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from '../../action/actions';
import PropTypes from 'prop-types';
import { withCookies } from 'react-cookie';
import { Form, Button, Alert } from 'react-bootstrap';
import '../../css/starting.css';

class Login extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch)
        this.state = {
            account: "",
            accountLength: 0,
            name: "",
            nameLength: 0,
            password: "",
            passwordLength: 0,
            passwordConfirmation: "",
            referenceDate: "",
            isMissSignup: false,
            isAccountError: true,
            accountErrMsg: "",
            isNameError: true,
            nameErrMsg: "",
            isReferenceError: true,
            referenceErrMsg: "",
            isPasswordError: true,
            passwordErrMsg: "",
            isConfirmationError: true,
            confirmationErrMsg: "",
            users: []
        }
    }

    componentDidMount() {
        // ユーザー重複バリデーション用
        fetch("http://localhost:8080/users", {
            method: "GET"
        }).then((response) => {
            if (response.status === 200) {
                response.json().then(json => {
                    // アカウントをStateへ保存
                    this.setState({ users: json })
                })
            } else {
                alert("通信エラー")
            }
        })
            .catch(error => console.log(error));
    }


    // アカウントState
    handleAccountChange(event) {
        const value = event.target.value;
        this.setState({
            account: value,
            accountLength: value.length
        });
        // 文字数カウント
        const element = document.getElementById('accountLen');
        if (value.length <= 5 || value.length > 20) {
            element.style.color = "red";
        } else {
            element.style.color = "green";
        }
        // アカウント重複
        let accountMatch = false
        this.state.users.forEach(user => {
            if (value == user.account) {
                accountMatch = true;
                return;
            }
        });

        // バリデーション
        if (value.match("[^a-zA-Z0-9]")) {
            this.setState({
                isAccountError: true,
                accountErrMsg: "アカウントは半角英数字のみ使えます"
            })
        } else if (value.length < 6 || value.length > 20) {

            this.setState({
                isAccountError: true,
                accountErrMsg: ""
            })
        } else if (accountMatch) {
            this.setState({
                isAccountError: true,
                accountErrMsg: "アカウントが重複しています"
            })
        } else {
            this.setState({
                isAccountError: false,
                accountErrMsg: ""
            })
            const emelentStyle = document.getElementById('formBasicAccount').style;
                emelentStyle.border = "";
        }
    }



    // ネームState
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({
            name: value,
            nameLength: value.length
        })
        const element = document.getElementById('nameLen');
        // 文字数カウント
        if (value.length <= 0 || value.length > 10) {
            element.style.color = "red";
        } else {
            element.style.color = "green";
        }
        // バリデーション
        if (value.match("[^ぁ-んァ-ヶ一-龠a-zA-Z 　ｂ-ｚ]")) {
            this.setState({
                isNameError: true,
                nameErrMsg: "名前に数字、記号は使えません"
            })
        } else {

            if (value.length <= 0 || value.length > 10) {
                this.setState({ isNameError: true })
            } else {
                this.setState({ isNameError: false })
                const emelentStyle = document.getElementById('formBasicName').style;
                emelentStyle.border = "";
            }

            this.setState({
                nameErrMsg: ""
            })
        }
    }
    // パスワードState
    handlePasswordChange(event) {
        const emelentStyle = document.getElementById('formBasicPassword').style;
                emelentStyle.border = "";
        const value = event.target.value;
        this.setState({
            password: value,
            passwordLength: value.length
        });
        const element = document.getElementById('passwordLen');
        // 文字数カウント
        if (value.length < 6 || value.length > 20) {
            element.style.color = "red";
            this.setState({ isPasswordError: true })
        } else {
            element.style.color = "green";
            this.setState({ isPasswordError: false })
        }

        const conElement = document.getElementById('passwordCheck');
        if (this.state.passwordConfirmation.length <= 0) {
            this.setState({
                confirmationErrMsg: "",
                isConfirmationError: true
            })
        } else if (this.state.passwordConfirmation == value) {
            conElement.style.color = "green";
            this.setState({
                confirmationErrMsg: "確認OK",
                isConfirmationError: false
            })
        } else {
            conElement.style.color = "red";
            this.setState({
                confirmationErrMsg: "パスワードが一致しません",
                isConfirmationError: true
            })
        }

    }
    // 確認用パスワードState
    handlePasswordConfirmationChange(event) {
        const emelentStyle = document.getElementById('formBasicPasswordConfirmation').style;
                emelentStyle.border = "";
        const value = event.target.value;
        this.setState({ passwordConfirmation: value })

        const element = document.getElementById('passwordCheck');
        if (value.length <= 0) {
            element.style.color = "red";
            this.setState({
                confirmationErrMsg: "入力してください",
                isConfirmationError: true
            })
        } else if (this.state.password == value) {
            element.style.color = "green";
            this.setState({
                confirmationErrMsg: "確認OK",
                isConfirmationError: false
            })
        } else {
            element.style.color = "red";
            this.setState({
                confirmationErrMsg: "パスワードが一致しません",
                isConfirmationError: true
            })
        }
    }
    // 始期日State
    handleDateChange(event) {
        const value = event.target.value;
        this.setState({
            referenceDate: value,
            isReferenceError: false
        })
        const emelentStyle = document.getElementById('formBasicReferenceDate').style;
                emelentStyle.border = "";
    }
    // 登録ボタン
    signupSubmit() {
        if (this.state.isNameError || this.state.isAccountError || this.state.isReferenceError || this.state.isPasswordError || this.state.isConfirmationError) {
            this.setState({ isMissSignup: true })
            if (this.state.account.length == 0) {
                this.setState({ accountErrMsg: "アカウントは必須です" })
                const emelentStyle = document.getElementById('formBasicAccount').style;
                emelentStyle.border = "2px solid red";
            }
            if (this.state.name.length == 0) {
                this.setState({ nameErrMsg: "入力してください" })
                const emelentStyle = document.getElementById('formBasicName').style;
                emelentStyle.border = "2px solid red";
            }
            if (this.state.referenceDate.length == 0) {
                this.setState({ referenceErrMsg: "基準日を設定してください" })
                const emelentStyle = document.getElementById('formBasicReferenceDate').style;
                emelentStyle.border = "2px solid red";
            }
            if (this.state.password.length == 0) {
                this.setState({ passwordErrMsg: "入力してください"})
                const emelentStyle = document.getElementById('formBasicPassword').style;
                emelentStyle.border = "2px solid red";
            }
            const element = document.getElementById('passwordCheck');
            if(this.state.passwordConfirmation.length == 0) {
                element.style.color = "red"
                this.setState({ confirmationErrMsg: "入力してください"})
                const emelentStyle = document.getElementById('formBasicPasswordConfirmation').style;
                emelentStyle.border = "2px solid red";
            }
        } else {
            const data = { account: this.state.account, name: this.state.name, password: this.state.password, referenceDate: this.state.referenceDate };
            fetch("http://localhost:8080/users/signup", {
                headers: {
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(data)
            }).then((response) => {
                if (response.status === 200) {
                    response.json().then(json => {
                        this.action.logAnimation();
                        setTimeout(() => {
                            this.action.login(json)
                        }, 1000);
                        // クッキーをログイン状態に変更する
                        const { cookies } = this.props;
                        cookies.set('user', json, { path: "/" });
                        cookies.set('isSignedIn', true, { path: "/" });
                    })
                } else {
                    this.setState({ isMissSignup: true })
                }
            })
        }
    }

    render() {
        return (
            <>

                <div className="signup-area">
                    {this.state.isMissSignup &&
                        <Alert variant="danger text-center" className="login-error-alert">
                            <p>登録に失敗しました</p>
                        </Alert>
                    }

                    <Form className="input-regist-form">
                        <Form.Group className="form-object" controlId="formBasicAccount">
                            <Form.Label className="form-label">アカウント</Form.Label>
                            <div className="form-error">
                                <p className="err-msg">
                                    {this.state.isAccountError && this.state.accountErrMsg}
                                </p>
                                <Form.Control className="" onChange={(event) => this.handleAccountChange(event)} value={this.state.account} type="text" placeholder="半角英数字のみ使えます" />
                                <p className="length-check"><span className="account-length" id="accountLen">{this.state.accountLength}</span>/20</p>
                            </div>
                        </Form.Group>
                        <Form.Group className="form-object" controlId="formBasicName">
                            <Form.Label className="form-label">氏名</Form.Label>
                            <div className="form-error">
                                
                                    {this.state.isNameError && <p className="err-msg">{this.state.nameErrMsg}</p>}
                                
                                <Form.Control className="" onChange={(event) => this.handleNameChange(event)} value={this.state.name} type="text" placeholder="記号は使えません" />
                                <p className="length-check mb-0"><span className="name-length" id="nameLen">{this.state.nameLength}</span>/10</p>
                            </div>
                        </Form.Group>
                        <Form.Group className="form-object" controlId="formBasicReferenceDate">
                            <Form.Label className="form-label">基準日</Form.Label>
                            <div className="form-error">
                                <p className="err-msg">
                                    {this.state.isReferenceError && this.state.referenceErrMsg}
                                </p>
                                <Form.Control className="" onChange={(event) => this.handleDateChange(event)} value={this.state.referenceDate} type="Date" />
                            </div>
                        </Form.Group>
                        <Form.Group className="form-object" controlId="formBasicPassword">
                            <Form.Label className="form-label">パスワード</Form.Label>
                            <div className="form-error">
                                <p className="err-msg">
                                    {this.state.isPasswordError && this.state.passwordErrMsg}
                                </p>
                                <Form.Control className="" onChange={(event) => this.handlePasswordChange(event)} value={this.state.password} type="password" placeholder="半角英数字と記号が使えます" />
                                <p className="length-check"><span className="password-length" id="passwordLen">{this.state.passwordLength}</span>/20</p>
                            </div>
                        </Form.Group>
                        <Form.Group className="form-object" controlId="formBasicPasswordConfirmation">
                            <Form.Label className="form-label">パスワード(確認用)</Form.Label>
                            <div className="form-error">
                                <Form.Control className="" onChange={(event) => this.handlePasswordConfirmationChange(event)} value={this.state.passwordConfirmation} type="password" />
                                <p className="length-check" id="passwordCheck">{this.state.confirmationErrMsg}</p>
                            </div>
                        </Form.Group>
                        <Button className="signupButton mt-3" onClick={() => this.signupSubmit()} variant="primary">
                            登録
                        </Button>
                    </Form>
                </div>
            </>
        )
    }
}

Login.propTypes = {
    dispatch: PropTypes.func,
    isSignedIn: PropTypes.bool,
    cookies: PropTypes.any
}

function mapStateToProps(state) {
    return state
}

export default withCookies(connect(mapStateToProps)(Login));
