import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../action/actions'
import PropTypes from 'prop-types';
import { withCookies } from 'react-cookie';
import '../../css/userInfo.css';
import moment from 'moment';
import Update from './update';

class UserInfo extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch)
        this.state = {
            isAccountMenuToggle: false
        }
    }



    // アカウント削除確認
    destroyTo() {
        const destroyAlert = document.getElementsById('destroyAlert')
        console.log(destroyAlert)
    }
    destroyAlert() {
        return (
            <div id="destroyAlert" className="destroy-alert display-none">
                <p>アカウントが削除されます。よろしいですか？</p>
                <button>Yes</button>  <button>No</button>
            </div>
        )
    }

    // アカウントメニューボタンクリック
    menuTggleChange() {
        this.setState({ isAccountMenuToggle: !this.state.isAccountMenuToggle })
    }
    tggleChange(bool) {
        this.setState({ isAccountMenuToggle: bool })
    }

    render() {
        return (
            <div className="user-info">
                <div className="container">
                    <p className="name">{this.props.user.name}さん</p>
                    <p className="reference-date">基準日：{moment(this.props.user.referenceDate).format('M月D日')}</p>
                    <button onClick={() => this.menuTggleChange()}>
                        {this.state.isAccountMenuToggle ? '元に戻す' : 'アカウントメニュー'}
                    </button>
                    {this.state.isAccountMenuToggle && <Update tggleChange={(bool) => this.menuTggleChange(bool)} />}

                </div>
                <div className="footer">

                    {!this.state.isAccountMenuToggle && <button onClick={() => this.handleLogout()}>ログアウト</button>}
                </div>
            </div>
        )
    }
}

UserInfo.propTypes = {
    dispatch: PropTypes.func,
    user: PropTypes.any,
    isSignedIn: PropTypes.bool,
    cookies: PropTypes.any
}

function mapStateToProps(state) {
    return state
}

export default withCookies(connect(mapStateToProps)(UserInfo));
