import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as actions from '../../action/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import '../../css/starting.css';
import { Form, Button, Alert } from 'react-bootstrap';

class Login extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch);
        this.state = {
            account: "",
            password: "",
            isAccountNull: false,
            isPasswordNull: false,
            isMissLogin: false
        }
    }


    // アカウントstate管理
    handleAccountChange(event) {
        const value = event.target.value;
        const bool = value.length < 1 ? true : false;
        this.setState({
            account: value,
            isAccountNull: bool
        })
        if (value.length > 0) {
            const elementStyle = document.getElementById('formBasicAccount').style
            elementStyle.border = ""
        }
    }
    // パスワードstate管理
    handlePasswordChange(event) {
        const value = event.target.value;
        const isEmpty = value.length < 1 ? true : false;
        this.setState({
            password: value,
            isPasswordNull: isEmpty
        })
        if (value.length > 0) {
            const elementStyle = document.getElementById('formBasicPassword').style
            elementStyle.border = ""
        }
    }
    // ログインボタン
    loginSubmit() {
        if (this.state.account.length > 0 && this.state.password.length > 0) {
            const data = { account: this.state.account, password: this.state.password };
            fetch("http://localhost:8080/users/login", {
                headers: {
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(data)
            })
                .then((response) => {
                    if (response.status === 200) {
                        response.json().then(json => {
                            this.action.logAnimation();
                            setTimeout(() => {
                                this.action.login(json)
                            }, 1000);
                            
                            // クッキーのログイン情報を変更する
                            const { cookies } = this.props;
                            cookies.set('user', json, { path: "/" });
                            cookies.set('isSignedIn', true, { path: "/" });
                            this.setState({
                                isMissLogin: false
                            })
                        })
                    } else {
                        this.setState({
                            isMissLogin: true,
                            password: ''
                        })
                    }
                })
                .catch(error => console.log(error));
        } else {
            if (this.state.account.length <= 0) {
                const elementStyle = document.getElementById('formBasicAccount').style
                elementStyle.border = "2px solid red"
                this.setState({
                    isAccountNull: true,
                })
            }
            if (this.state.password.length <= 0) {
                const elementStyle = document.getElementById('formBasicPassword').style
                elementStyle.border = "2px solid red"
                this.setState({ isPasswordNull: true })
            }
        }
    }




    render() {
        return (
            <div className="loginMain">
                {this.state.isMissLogin &&
                    <Alert variant="danger py-2 text-center" className="login-error-alert">
                        <p>アカウントまたはパスワードが間違っています</p>
                    </Alert>
                }
                <Form>
                    <Form.Group className="mb-3" controlId="formBasicAccount">
                        <Form.Label>Account</Form.Label>
                        <Form.Control onChange={(event) => this.handleAccountChange(event)} value={this.state.account} type="text" placeholder="アカウント" />
                        <p className="err-msg">
                            {this.state.isAccountNull && 'アカウントを入力してください'}
                        </p>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control onChange={(event) => this.handlePasswordChange(event)} value={this.state.password} type="password" placeholder="パスワード" />
                        <p className="err-msg">
                            {this.state.isPasswordNull && 'パスワードを入力してください'}
                        </p>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicCheckbox">
                    </Form.Group>
                    <Button className="loginButton" onClick={() => this.loginSubmit()} variant="primary">
                        ログイン
                    </Button>
                </Form>
            </div>
        )
    }
}

Login.propTypes = {
    dispatch: PropTypes.func,
    user: PropTypes.any,
    isSignedIn: PropTypes.bool,
    cookies: PropTypes.any,
}

function mapStateToProps(state) {
    return state;
}

export default withCookies(connect(mapStateToProps)(Login));