import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../action/actions'
import PropTypes from 'prop-types';
import { withCookies } from 'react-cookie';
import '../../css/userDelete.css';
import { Button } from 'react-bootstrap';

class UserDelete extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch)
        this.state = {
            isDestroyAlert: false
        }
    }



    // アカウント削除
    destroyTo() {
        if (confirm("アカウントに紐づいた収支情報も削除されます")) {
            const userId = this.props.user.id;
            fetch("http://localhost:8080/users/delete/" + userId, {
                headers: {
                    'Content-Type': 'application/json'
                },
                method: "DELETE"
            })
                .then((response) => {
                    if (response.status === 200) {
                        alert("アカウントが削除されました")
                        this.action.logout();
                        // クッキー情報変更
                        const { cookies } = this.props;
                        cookies.set('isSignedIn', '', { path: "/" });
                        cookies.set('user', '[]', { path: "/" });

                    } else {
                        alert("削除失敗")
                    }
                })
                .catch(error => console.log(error));
        } else {
            alert("キャンセルしました")
            this.destroyAlertOn()
        }
    }
    destroyAlert() {
        return (
            <div id="destroyAlert" className="destroy-alert">
                <p>アカウントが削除されます。よろしいですか？</p>
                <Button className="btn btn-danger" onClick={() => this.destroyTo()}>アカウント削除</Button>
                <Button className="btn btn-light ms-3" onClick={() => this.destroyAlertOn()}>キャンセル</Button>
            </div>
        )
    }

    destroyAlertOn() {
        this.setState({
            isDestroyAlert: !this.state.isDestroyAlert
        })
    }

    destroyLink() {
        return (
            <a className="link destroy-link" onClick={() => this.destroyAlertOn()}>アカウント削除</a>
        )
    }

    render() {
        return (
            <div className="destroy-main">
                {this.state.isDestroyAlert ?
                    this.destroyAlert() :
                    this.destroyLink()}
            </div>
        )
    }
}

UserDelete.propTypes = {
    dispatch: PropTypes.func,
    user: PropTypes.any,
    isSignedIn: PropTypes.bool,
    cookies: PropTypes.any
}

function mapStateToProps(state) {
    return state
}

export default withCookies(connect(mapStateToProps)(UserDelete));
