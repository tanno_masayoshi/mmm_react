import React, { Component } from 'react';
import { Doughnut } from 'react-chartjs-2';
import '../../css/userChart.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../action/actions'
import PropTypes from 'prop-types';
import { withCookies } from 'react-cookie';
import moment from 'moment';

function UserExpenseIncomeChart(start, end, incomes, expenses) {
    start = moment(start).format("MM/DD")
    end = moment(end).format("MM/DD")
    let incomeTotal = 0;
    let expenseTotal = 0;

    incomes.map((income) => {
        incomeTotal += income.amount;
    })

    expenses.map((expense) => {
        expenseTotal += expense.price;
    })
    let totalPrice = incomeTotal - expenseTotal;

    const graphdata = {
        datasets: [
            {
                data: [incomeTotal, expenseTotal],
                backgroundColor: ['#B9D8F7', '#FFE5EC'],
            },
        ],
        labels: ['収入', '支出'],
    };

    return (
        <div>
            <div className="chart-area">
                <div className="text-area">
                    <h5>今月のトータル収支</h5>
                    <p>{start} ~ {end}</p>
                    <h5 style={{
                        color: totalPrice < 0 && 'red'
                    }}>
                        \{totalPrice}円
                    </h5>
                </div>
                <Doughnut data={graphdata} />
            </div>
        </div>
    );
}

class Chart extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch)
        this.state = {
            start: "",
            end: "",
            incomes: [],
            expenses: [],
        }
    }

    daySet() {
        let today = new Date()
        today = new Date(today.getFullYear(), today.getMonth(), today.getDate())
        let baseDate = moment(this.props.user.referenceDate).toDate();
        const year = new Date().getFullYear()
        const month = (new Date().getMonth())

        // 基準日が月初かどうか確認する
        // 月末の日付
        let lastDay = new Date(baseDate.getFullYear(), baseDate.getMonth() + 1, 0);
        // 翌月基準日用変数
        let nextMonthDate = new Date()
        if (lastDay.getDate() == baseDate.getDate()) {
            //１か月の基準を月末から翌月末前日に変更
            baseDate = new Date(year, month, 0);
            nextMonthDate = new Date(baseDate.getFullYear(), baseDate.getMonth() + 2, -1);

            // 今月の基準日が未来だった場合は1か月基準日を戻す
            if (baseDate > today) {
                baseDate = new Date(baseDate.getFullYear(), baseDate.getMonth() - 1, 0);
                nextMonthDate = new Date(baseDate.getFullYear(), baseDate.getMonth() + 2, 0)
            }
        } else {
            //１か月の基準を同日から翌月前日に変更
            baseDate = new Date(year, month, baseDate.getDate());
            nextMonthDate = new Date(baseDate.getFullYear(), baseDate.getMonth() + 1, baseDate.getDate() - 1);

            // 今月の基準日が未来だった場合は1か月基準日を戻す
            if (baseDate > today) {
                baseDate = new Date(baseDate.getFullYear(), baseDate.getMonth() - 1, baseDate.getDate());
                nextMonthDate = new Date(nextMonthDate.getFullYear(), nextMonthDate.getMonth() - 1, nextMonthDate.getDate())
            }
        }

        this.setState({
            start: baseDate,
            end: nextMonthDate
        })

        this.getDates(baseDate, nextMonthDate)
    }

    componentDidMount() {
        this.daySet();
    }

    getDates(start, end) {
        start = moment(start).format("yyyy-MM-DD")
        end = moment(end).format("yyyy-MM-DD")
        // 支出取得
        fetch("http://localhost:8080/expense/getall/" + this.props.user.id + "/" + start + "/" + end, {
            method: "GET"
        })
            .then((response) => {
                response.json().
                    then(json => {
                        this.setState({
                            expenses: json
                        })
                    })
            })

        // 収入取得
        fetch("http://localhost:8080/incomes/getall/" + this.props.user.id + "/" + start + "/" + end, {
            method: "GET"
        })
            .then((response) => {
                response.json()
                    .then(json => {
                        this.setState({
                            incomes: json
                        })
                    })
            })
    }

    render() {
        return (
            <div>
                {UserExpenseIncomeChart(this.state.start, this.state.end, this.state.incomes, this.state.expenses)}
            </div>
        )
    }
}


Chart.propTypes = {
    isSignedIn: PropTypes.bool,
    dispatch: PropTypes.func,
    cookies: PropTypes.any,
    useState: PropTypes.func,
    isLoginAnimation: PropTypes.bool,
    user: PropTypes.object
}

function mapStateToProps(state) {
    return state
}

export default withCookies(connect(mapStateToProps)(Chart));