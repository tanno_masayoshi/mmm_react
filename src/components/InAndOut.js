import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import { bindActionCreators } from 'redux'
import * as actions from '../action/actions';
import { Container, Row } from 'react-bootstrap';

import AddIncome from './incomes/addIncome';
import EditIncome from './incomes/edit';
import MonthlyList from './inAndOut/monthly_list';
import AddExpense from './expenses/add';
import EditExpense from './expenses/edit';
import '../css/inAndOut.css';

class InAndOut extends Component {
  constructor(props) {
    super(props)
    const { dispatch } = props;
    this.action = bindActionCreators(actions, dispatch);
  }

  componentDidMount() {
    fetch("http://localhost:8080/expense/category/" + this.props.user.id, {
      method: "GET"
    })
      .then((response) => {
        response.json().
          then(json => {
            this.action.updateCategoryList(json)
          })
      })
  }

  selectPage() {
    const { cookies } = this.props;
    switch (cookies.get("page")) {
      case undefined: return <MonthlyList />;
      case "MonthlyList": return <MonthlyList />;
      case "ExpenseAdd": return <AddExpense />;
      case "ExpenseEdit": return <EditExpense />;
      case "IncomeAdd": return <AddIncome />;
      case "IncomeEdit": return <EditIncome />;
    }
  }

  render() {
    const { cookies } = this.props;

    const selectMonthlyList = () => {
      cookies.set("page", "MonthlyList", { path: "/" });
    }

    const selectExpenseAddPage = () => {
      cookies.set("page", "ExpenseAdd", { path: "/" });
    }

    const selectIncomeAddPage = () => {
      cookies.set("page", "IncomeAdd", { path: "/" });
    }

    return (
      <Container>
        <Row>
            <nav className="page_nav">
              <div className="tab" id="MonthlyList_tab" onClick={selectMonthlyList}>
                <p>収支リスト</p>
              </div>
              <div className="tab" id="ExpenseAdd_tab" onClick={selectExpenseAddPage}>
                <p>支出登録</p>
              </div>
              <div className="tab" id="IncomeAdd_tab" onClick={selectIncomeAddPage}>
                <p>収入登録</p>
              </div>
            </nav>
        </Row>
        <Row>
          {this.selectPage()}
        </Row>
      </Container>
    )
  }
}

InAndOut.propTypes = {
  dispatch: PropTypes.func,
  isSignedIn: PropTypes.bool,
  cookies: PropTypes.any,
  user: PropTypes.any
}

function mapStateToProps(state) {
  return state
}
export default withCookies(connect(mapStateToProps)(InAndOut));
