import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import moment from 'moment';
import { bindActionCreators } from 'redux'
import * as actions from '../../action/actions';

import ExpenseListElement from '../expenses/list_element.js';
import IncomeListElement from '../incomes/list_element.js';
import '../../css/monthlyListElement.css'

class MonthlyListElement extends Component {
  constructor(props) {
    super(props)
    const { dispatch } = props;
    this.action = bindActionCreators(actions, dispatch);
    this.state = {
      isReading: false
    }
  }

  expensesSum() {
    let sum = 0;
    this.props.expenseArray.forEach(function (expense) {
      sum += expense.price;
    });
    return sum;
  }

  isReadingChange() {
    this.setState({ isReading: !this.state.isReading })
  }

  render() {
    const format = this.props.format;
    const expenseArrayMuched = this.props.expenseArray.filter(function (expense) {
      return moment(expense.buyDate).format('yyyy-MM-DD') == format;
    });
    const incomeArrayMuched = this.props.incomeArray.filter(function (income) {
      return moment(income.incomeDate).format('yyyy-MM-DD') == format;
    });

    let expensesSum = 0;
    expenseArrayMuched.forEach(function (expense) {
      expensesSum += expense.price;
    });

    let incomesSum = 0;
    incomeArrayMuched.forEach(function (income) {
      incomesSum += income.amount;
    });

    const expenselist = expenseArrayMuched.map((expense) => (
      <ExpenseListElement
        expenseId={expense.id}
        category={expense.category.name}
        description={expense.description}
        price={expense.price}
        buyDate={expense.buyDate}
        key={expense.id}
      />
    ));
    const incomelist = incomeArrayMuched.map((income) => (
      <IncomeListElement
        incomeId={income.id}
        category={income.category}
        amount={income.amount}
        incomeDate={income.incomeDate}
        key={income.id}
      />
    ));

    return (
      <tr className="tr-main">
        <th>
          <span>{this.props.month}/</span>
          <span>{this.props.date}/</span>
          <span>({this.props.day})</span>
        </th>
        <td className="td-main">
          {this.state.isReading ? expenselist : ''}
          <span>{this.state.isReading ?  '合計' + expensesSum + '円' : expensesSum + '円'}</span>
        </td>
        <td className="td-main">
          {this.state.isReading ? incomelist : ''}
          <span>{this.state.isReading ?  '合計' + incomesSum + '円' : incomesSum + '円'}</span>
        </td>
        <td>
          <button className="btn-c" onClick={() => this.isReadingChange()}>{this.state.isReading ? '△' : '▽'}</button>
        </td>
      </tr>
    )}
}

MonthlyListElement.propTypes = {
  isReading: PropTypes.bool,
  month: PropTypes.string,
  date: PropTypes.number,
  day: PropTypes.string,
  format: PropTypes.any,
  dispatch: PropTypes.func,
  expenseArray: PropTypes.array,
  incomeArray: PropTypes.array,
}

function mapStateToProps(state) {
  return state
}
export default connect(mapStateToProps)(MonthlyListElement);
