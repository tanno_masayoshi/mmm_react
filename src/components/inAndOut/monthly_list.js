import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';

import { bindActionCreators } from 'redux'
import * as actions from '../../action/actions';
import moment from 'moment';

import MonthlyListElement from './monthly_list_element.js';
import MonthlyGraph from './MonthlyGraph';
import '../../css/monthlyList.css'
// import Plans from '../../Plans';
import CategoryIndex from '../exCategories/categoryIndex'



class MonthlyList extends Component {
  constructor(props) {
    super(props)

    const { dispatch } = props;
    this.action = bindActionCreators(actions, dispatch);
    this.state = {
      selectMonth: 0,
      monthlyList: []
    }
  }

  componentDidMount() {
    this.setList();
    document.getElementById('MonthlyList_tab').style.backgroundColor = 'white';

    document.getElementById('ExpenseAdd_tab').style.backgroundColor = '#CCC';
    document.getElementById('IncomeAdd_tab').style.backgroundColor = '#CCC';
  }

  //ページ繰り修正-stateのselectMonth変更前の値を参照したうえで増減する引数pickPageを設定
  setList(pickPage) {
    if (pickPage == null || pickPage == '') {
      pickPage = 0
    }

    // const now = new Date();
    // const year = now.getFullYear();
    // const month = now.getMonth();
    // const date = now.getDate();
    // const payday = moment(this.props.user.referenceDate).format('DD');

    let start = "";
    let end = "";

    let today = new Date()
    today = new Date(today.getFullYear(), today.getMonth(), today.getDate())
    let baseDate = moment(this.props.user.referenceDate).toDate();
    const year = new Date().getFullYear()
    const month = (new Date().getMonth())

    // 基準日が月初かどうか確認する
    // 月末の日付
    let lastDay = new Date(baseDate.getFullYear(), baseDate.getMonth() + 1, 0);
    // 翌月基準日用変数
    let nextMonthDate = new Date()
    if (lastDay.getDate() == baseDate.getDate()) {
      //１か月の基準を月末から翌月末前日に変更
      baseDate = new Date(year, month, 0);
      nextMonthDate = new Date(baseDate.getFullYear(), baseDate.getMonth() + 2, -1);

      // 今月の基準日が未来だった場合は1か月基準日を戻す
      if (baseDate > today) {
        baseDate = new Date(baseDate.getFullYear(), baseDate.getMonth() - 1, 0);
        nextMonthDate = new Date(baseDate.getFullYear(), baseDate.getMonth() + 2, 0)
      }
    } else {
      //１か月の基準を同日から翌月前日に変更
      baseDate = new Date(year, month, baseDate.getDate());
      nextMonthDate = new Date(baseDate.getFullYear(), baseDate.getMonth() + 1, baseDate.getDate() - 1);

      // 今月の基準日が未来だった場合は1か月基準日を戻す
      if (baseDate > today) {
        baseDate = new Date(baseDate.getFullYear(), baseDate.getMonth() - 1, baseDate.getDate());
        nextMonthDate = new Date(nextMonthDate.getFullYear(), nextMonthDate.getMonth() - 1, nextMonthDate.getDate())
      }
    }

    start = new Date(baseDate.getFullYear(), baseDate.getMonth() + pickPage, baseDate.getDate());
    end = new Date(nextMonthDate.getFullYear(), nextMonthDate.getMonth() + pickPage, nextMonthDate.getDate());
    // const payday = moment(start).format('DD');

    // if (date < payday) {
    //   start = moment(new Date(year, month, payday)).add(-1 + pickPage, 'M');
    //   end = moment(new Date(year, month, payday)).add(pickPage, 'M').subtract(1, 'd');
    // } else {
    //   start = moment(new Date(year, month, payday)).add(pickPage, 'M');
    //   end = moment(new Date(year, month, payday)).add(1 + pickPage, 'M').subtract(1, 'd');
    // }

    fetch("http://localhost:8080/expense/getall/" + this.props.user.id + "/" + moment(start).format("yyyy-MM-DD") + "/" + moment(end).format("yyyy-MM-DD"), {
      method: "GET"
    })
      .then((response) => {
        response.json().
          then(json => {
            this.action.updateExpenseList(json);
          })
      })

    fetch("http://localhost:8080/incomes/getall/" + this.props.user.id + "/" + moment(start).format("yyyy-MM-DD") + "/" + moment(end).format("yyyy-MM-DD"))
      .then((response) => {
        response.json().
          then(json => {
            this.action.getIncomes(json);
          })
      })

    this.action.updateListTerm(moment(start).format("yyyy-MM-DD"), moment(end).format("yyyy-MM-DD"));

    let localDate = start;
    const monthlyList = [];
    for (let step = 1; localDate <= end; step++) {
      let day = "";
      switch (localDate.getDay()) {
        case 0: day = "日";
          break;

        case 1: day = "月";
          break;

        case 2: day = "火";
          break;

        case 3: day = "水";
          break;

        case 4: day = "木";
          break;

        case 5: day = "金";
          break;

        case 6: day = "土";
          break;
      }

      const object = { month: moment(new Date(localDate)).format("MM") , date: localDate.getDate(), day: day, format: moment(localDate).format("yyyy-MM-DD"), key: step };
      monthlyList.push(object);
      localDate = new Date(localDate.getFullYear(), localDate.getMonth(), localDate.getDate() + 1)
    }

    this.setState({ monthlyList: monthlyList });
    this.action.getMonthlyList(monthlyList);
  }

  render() {

    //expense日ごと総額
    let totalPriceArray = [];
    const expenseArray = this.props.expenseArray
    this.state.monthlyList.forEach(function (day) {
      const expenseArrayMuched = expenseArray.filter(function (expense) {
        return moment(expense.buyDate).format('yyyy-MM-DD') == day.format;
      });

      let TexpensesSum = 0;
      expenseArrayMuched.forEach(function (expense) {
        TexpensesSum += expense.price;
      });
      totalPriceArray.push(TexpensesSum);
    });
    //ここまで

    //income日ごと総額
    let totalAmountArray = [];
    const incomeArray = this.props.incomes

    this.state.monthlyList.forEach(function (day) {
      const incomeArrayMuched = incomeArray.filter(function (income) {
        return moment(income.incomeDate).format('yyyy-MM-DD') == day.format;
      });

      let incomesSum = 0;
      incomeArrayMuched.forEach(function (income) {
        incomesSum += income.amount;
      });
      totalAmountArray.push(incomesSum)
    });
    //ここまで

    //ページ繰り修正-setListについて、現在のstateを基準にsetListに値を渡したのちにstate変更する
    const selectMonthBack = () => {
      this.setList(this.state.selectMonth - 1)
      this.setState({ selectMonth: (this.state.selectMonth - 1) });
    }

    const selectMonthNext = () => {
      this.setList(this.state.selectMonth + 1);
      this.setState({ selectMonth: (this.state.selectMonth + 1) });
    }

    const selectMonthDefault = () => {
      this.setList(0);
      this.setState({ selectMonth: 0 });
    }

    let expensesSum = 0;
    this.props.expenseArray.forEach(function (expense) {
      expensesSum += expense.price;
    });

    let incomesSum = 0;
    this.props.incomes.forEach(function (income) {
      incomesSum += income.amount;
    });

    const inAndOutlist = this.state.monthlyList.map((element) => (
      <MonthlyListElement
        month={element.month}
        date={element.date}
        day={element.day}
        format={element.format}
        incomeArray={this.props.incomes}
        key={element.key}
      />
    ));

    return (
      <div className="graph-and-list">
        <div className="monthly-graph">
          <div className="select-month-button">
            <button className="btn-a" onClick={selectMonthBack}>◀前月に戻る</button>&emsp;
            <button className="btn-d" onClick={selectMonthDefault}>今月</button>&emsp;
            <button className="btn-b" onClick={selectMonthNext}>次月に進む▶</button>&emsp;
          </div>
          <MonthlyGraph totalPriceArray={totalPriceArray} totalAmountArray={totalAmountArray} />
          <span>期間の支出合計:&emsp;</span><span className="expenseValue">{expensesSum}円&emsp;</span>
          <span>/&emsp;期間の収入合計:&emsp;</span><span className="incomeValue">{incomesSum}円&emsp;</span>

          <div className="category-index">
            <h2>支出カテゴリ別合計金額</h2>
            <CategoryIndex selectMonth={this.state.selectMonth} />
          </div>

        </div>
        <div className="list-wrapper">
          <div className="inAndOutList">
            <table>
              <thead>
                <tr className="tr-main">
                  <th className="th-a">日にち</th>
                  <th className="th-b">支出</th>
                  <th className="th-c">収入</th>
                </tr>
              </thead>
              <tbody>
                {inAndOutlist}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
}

MonthlyList.propTypes = {
  dispatch: PropTypes.func,
  expenseArray: PropTypes.array,
  user: PropTypes.any,
  incomes: PropTypes.array,
  termStart: PropTypes.any
}

function mapStateToProps(state) {
  return state
}
export default connect(mapStateToProps)(MonthlyList);
