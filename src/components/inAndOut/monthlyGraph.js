import React, { Component } from 'react'
import { Bar } from 'react-chartjs-2';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../action/actions';
import '../../css/monthlyGraph.css';

class MonthlyGraph extends Component {
  constructor(props) {
    super(props)
    const { dispatch } = props;
    this.action = bindActionCreators(actions, dispatch)
    this.state = {
      data: [],
    }
  }
  render() {
    let totalList = []
    for (let i = 0; i < this.props.totalPriceArray.length; i++) {
      let amountSum = this.props.totalAmountArray.slice(0, i + 1).reduce(function (sum, element) {
        return sum + element;
      }, 0);
      let priceSum = this.props.totalPriceArray.slice(0, i + 1).reduce(function (sum, element) {
        return sum + element;
      }, 0);
      totalList.push(amountSum - priceSum)
    }
    let dayAndDate =[]
    for (let i = 0; i < this.props.monthlyList.length; i++) {
      dayAndDate.push(this.props.monthlyList.map((element) => (element.date))[i] + "(" + this.props.monthlyList.map((element) => (element.day))[i] + ")")
    }

    const graphData = {
      labels: dayAndDate,
      datasets: [
        {
          data: this.props.totalPriceArray,
          backgroundColor: 'rgba(240,100,110,1)',
          label: '支出額',
        },
        {
          type: 'line',
          fill: false,
          data: totalList,
          backgroundColor: 'rgba(70,190,190,1)',
          label: '期間内の収支差額(残高)',
        }
      ]
    }
    const graphOption = {
      label: 'My First dataset',
      enabled: false,
      //animation: false,
      fill: true,
      lineTension: 0.2,
      borderCapStyle: 'round',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'square',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#eee',
      pointBorderWidth: 10,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 1,
      pointRadius: 1,
      pointHitRadius: 10,
      responsive: true,


    }
    /*収支総計
    const totalPrice = this.props.totalPriceArray.reduce(function(sum, element){
      return sum + element;    }, 0);
    const totalAmount = this.props.totalAmountArray.reduce(function(sum, element){
      return sum + element;    }, 0);
    */
    let startDate = this.props.monthlyList.map((element) => (element.format))[0];
    let endDate = this.props.monthlyList.map((element) => (element.format))[this.props.monthlyList.map((element) => (element.format)).length - 1];

    return (
      <div className="monthlyGraph">
        <div id="graphSheet" >
          <div className="title">&emsp;【 {startDate} 】から【 {endDate} 】まで</div>
          <Bar data={graphData} options={graphOption}
            width={700}
            height={150} />
        </div>
      </div>
    );
  }
}


MonthlyGraph.propTypes = {
  dispatch: PropTypes.func,
  data: PropTypes.any,
  totalAmountArray: PropTypes.array,
  totalPriceArray: PropTypes.array,
  monthlyList: PropTypes.array,
}

function mapStateToProps(state) {
  return state
}
export default connect(mapStateToProps)(MonthlyGraph);
