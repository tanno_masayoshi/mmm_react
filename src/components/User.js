import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../action/actions';
import PropTypes from 'prop-types';
import { withCookies } from 'react-cookie';
import '../css/user.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import 'react-tabs/style/react-tabs.css';
import '../css/user.css';
import Logout from './users/logout';
import Update from './users/update';
import CategoryEdit from './exCategories/edit';
import Chart from '../components/users/chart';
import UserDelete from './users/userDelete';

class User extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch)
        this.state = {
            isAccountMenu: false,
            isAccountList: false,
            isAccountInfo: false,
            isCategoryInfo: false,
            isChart: true,
            isChartOpen: true
        }
    }

    handeAccountMenuChangeOpen() {
        if (!this.state.isAccountMenu) {
            this.setState({
                isAccountMenu: !this.state.isAccountMenu
            })

            setTimeout(() => {
                this.setState({
                    isAccountList: !this.state.isAccountList
                })
            }, 200);
        } else {
            this.setState({
                isAccountList: !this.state.isAccountList
            })

            setTimeout(() => {
                this.setState({
                    isAccountMenu: !this.state.isAccountMenu
                })
            }, 1000);
        }
    }

    submitAccountInfo() {
        this.setState({
            isAccountInfo: false
        })
        this.chartChange(false, false)
    }
    submitCategoryInfo() {
        this.setState({
            isCategoryInfo: false
        })
        this.chartChange(false, false)
    }

    accountInfo() {
        this.setState({
            isCategoryInfo: false,
            isAccountInfo: !this.state.isAccountInfo
        })
        this.chartChange(!this.state.isAccountInfo, false)
    }
    categoryInfo() {
        this.setState({
            isAccountInfo: false,
            isCategoryInfo: !this.state.isCategoryInfo
        })
        this.chartChange(false, !this.state.isCategoryInfo)
    }

    selectMenu(event) {
        const element = document.getElementById(event.target.id)
        element.style.boxShadow = '0 10px 25px 0 rgba(0, 0, 0, .5)';
    }
    unSelectMenu(event) {
        const element = document.getElementById(event.target.id)
        element.style.boxShadow = '';
    }

    accountMouse() {
        const element = document.getElementById('accountPropaty');
        element.style.backgroundColor = 'rgba(255, 255, 255, 0.9)';
        document.getElementById('accountPropaty').style.color = "black"
    }
    accountMouseOut() {
        const element = document.getElementById('accountPropaty');
        element.style.backgroundColor = '';
        const parentElement = document.getElementById('accountPropaty');
        parentElement.style.color = ''
    }

    chartChange(account, category) {
        if ((!account) && (!category)) {
            this.setState({ isChart: true })

            setTimeout(() => {
                this.setState({ isChartOpen: true })
            }, 1400);
        } else {
            this.setState({ isChartOpen: false })

            setTimeout(() => {
                this.setState({ isChart: false })
            }, 1500);
        }
    }

    render() {

        return (
            <div>
                <div id="accountPropaty" className="account-propaty" style={{
                    transition: '0.5s',
                    color: this.state.isAccountMenu && "black",
                    backgroundColor: this.state.isAccountMenu && 'rgba(255, 255, 255, 1)'
                }}
                    onClick={() => this.handeAccountMenuChangeOpen()}
                    onMouseOver={() => this.accountMouse()}
                    onMouseOut={() => this.accountMouseOut()}>
                    <div className="name-area" >
                        {this.props.user.name} さん
                    </div>
                </div>
                {this.state.isAccountMenu &&
                    <div className="account-lists" style={{
                        transition: '0.5s',
                        opacity: this.state.isAccountList ? '1' : '0'
                    }}>
                        <div className="close-bar"> <p onClick={() => this.handeAccountMenuChangeOpen()} className="closeButton">x</p></div>
                        <div className="main-content" >
                            <div className="account-menu-list">
                                {
                                    this.state.isChart &&
                                    <div className="chart-area" style={{
                                        transition: "1s",
                                        maxHeight: this.state.isChartOpen ? "500px" : "0px"
                                    }}>
                                        <Chart />
                                    </div>
                                }
                                アカウントメニュー
                                <ul className="menu-bar">
                                    <div className="content-group" style={{
                                        boxShadow: this.state.isAccountInfo && '0 10px 25px 0 rgba(0, 0, 0, .5)'
                                    }}>
                                        <li id="accountEditTitle" onClick={() => this.accountInfo()} onMouseOver={(event) => this.selectMenu(event)} onMouseOut={(event) => this.unSelectMenu(event)}>
                                            {this.state.isAccountInfo ? '▲' : '▼'}
                                            アカウント情報の編集
                                        </li>
                                        <div className="account-edit edit-parts" style={{
                                            transition: '0.5s',
                                            maxHeight: this.state.isAccountInfo ? "80vh" : "0"
                                        }}>
                                            <Update submitSuccess={() => this.submitAccountInfo()} />
                                        </div>
                                    </div>
                                    <div className="content-group" style={{
                                        boxShadow: this.state.isCategoryInfo && '0 10px 25px 0 rgba(0, 0, 0, .5)'
                                    }}>
                                        <li id="categoryEditTitle" onClick={() => this.categoryInfo()} onMouseOver={(event) => this.selectMenu(event)} onMouseOut={(event) => this.unSelectMenu(event)}>
                                            {this.state.isCategoryInfo ? '▲' : '▼'}
                                            支出用カテゴリの管理
                                        </li>
                                        <div className="category-edit edit-parts" style={{
                                            transition: '0.5s',
                                            maxHeight: this.state.isCategoryInfo ? "80vh" : "0"
                                        }}>
                                            <CategoryEdit submitSuccess={() => this.submitCategoryInfo()} />
                                        </div>
                                    </div>
                                </ul>
                            </div>
                        </div>
                        <div className="footer">
                            <Logout />
                            <UserDelete/>
                        </div>
                    </div>
                }
            </div>
        )
    }
}

User.propTypes = {
    dispatch: PropTypes.func,
    isSignedIn: PropTypes.bool,
    user: PropTypes.any,
    cookies: PropTypes.any
}

function mapStateToProps(state) {
    return state
}

export default withCookies(connect(mapStateToProps)(User));