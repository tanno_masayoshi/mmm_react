import React, { Component } from 'react';
import { Pie } from 'react-chartjs-2';
import '../../css/userChart.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../action/actions'
import PropTypes from 'prop-types';
import { withCookies } from 'react-cookie';
import moment from 'moment';

function UserExpenseIncomeChart(expenses, categories) {
    let categoryList = [];
    let colors = [];

    categories.map((category) => {
        categoryList.push({"id": category.id, "name": category.name, "total": 0 });
    })

    expenses.map((expense) => {
        categoryList.map((category) => {
            if (category["id"] == expense.categoryId) {
                category["total"] += expense.price
            }
        })
    })
    let priceData = []
    let nameData = []
    categoryList.map((category) => {
        priceData.push(category["total"]);
        nameData.push(category["name"]);
    })

    for(let i= 0; i < categoryList.length; i++) {
        colors.push("hsl(" + (((2001 * i) % 360) + 1)  + "," + ((333 * i) % 50 + 50) +"%,"+((333 * i) % 60 + 40)+ "%)");
    }

    const graphdata = {
        datasets: [
            {
                data: priceData,
                backgroundColor: colors,
            },
        ],
        labels: nameData,
    };
    const option = {
        animation: false
    }

    return (
        <div className="chart-graph">
            
            <div className="chart-area">
                <Pie data={graphdata} options={option} />
            </div>
        </div>
    );
}

class CategoryIndex extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch)
        this.state = {
            start: "",
            end: "",
            categories: this.props.categoryArray,
            expenses: []
        }
    }

    getDates() {
        let start = moment(this.props.termStart).format("yyyy-MM-DD")
        let end = moment(this.props.termEnd).format("yyyy-MM-DD")
        // 支出取得
        fetch("http://localhost:8080/expense/getall/" + this.props.user.id + "/" + start + "/" + end, {
            method: "GET"
        })
            .then((response) => {
                response.json().
                    then(json => {
                        this.setState({
                            expenses: json
                        })
                    })
            })
    }

    render() {
        return (
            <div>
                {UserExpenseIncomeChart(this.props.expenseArray, this.props.categoryArray)}
            </div>
        )
    }
}


CategoryIndex.propTypes = {
    isSignedIn: PropTypes.bool,
    dispatch: PropTypes.func,
    cookies: PropTypes.any,
    useState: PropTypes.func,
    isLoginAnimation: PropTypes.bool,
    user: PropTypes.object,
    termStart: PropTypes.string,
    termEnd: PropTypes.string,
    selectMonth: PropTypes.any,
    categoryArray: PropTypes.any,
    expenseArray: PropTypes.any
}

function mapStateToProps(state) {
    return state
}

export default withCookies(connect(mapStateToProps)(CategoryIndex));