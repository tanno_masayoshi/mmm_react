import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../action/actions';
import PropTypes from 'prop-types';
import { withCookies } from 'react-cookie';
import '../../css/categories.css';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Form, Button, Alert } from 'react-bootstrap';
import 'react-tabs/style/react-tabs.css';


class CategoryEdit extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch)
        this.state = {
            categoryName: "",
            isCategorySave: false,
            isSelectCategory: false,
            category: [],
            categoryLen: 0,
            isAddCategoryValid: false,
            editCategoryLen: 0,
            isEditCategory: false,
            currentCategory: "",
            isEditCategoryMatch: false,
            isAddCategoryMatch: false
        }
    }

    getExpenses() {
        if ((this.props.start != null) && (this.props.end != null)) {
            fetch("http://localhost:8080/expense/getall/" + this.props.user.id + "/" + this.props.start + "/" + this.props.end, {
                method: "GET"
            })
                .then((response) => {
                    response.json().
                        then(json => {
                            this.action.updateExpenseList(json)
                        })
                })
        }
    }

    componentDidMount() {
        fetch("http://localhost:8080/expense/category/" + this.props.user.id, {
            method: "GET"
        })
            .then((response) => {
                response.json().
                    then(json => {
                        this.action.updateCategoryList(json)
                    })
            })
    }

    dropCategory(id) {
        if (confirm("削除すると登録された支出も削除されます。よろしいですか？")) {
            fetch("http://localhost:8080/expense/category/delete/" + id + "/" + this.props.user.id, {
                headers: {
                    'Content-Type': 'application/json'
                },
                method: "DELETE"
            }).then((response) => {
                if (response.status === 200) {
                    response.json().then(json => {
                        this.action.updateCategoryList(json);
                    })
                    this.handleBoxChange();

                    this.getExpenses();
                } else {
                    alert("失敗しました")
                }
            }).catch(error => console.log(error));
        } else {
            alert("キャンセルしました。")
        }

    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({
            categoryName: value,
            isCategorySave: false,
            categoryLen: value.length
        })

        // 重複バリデーション
        let categoryMatch = false
        this.props.categoryArray.map((category) => {
            if (category.name == value) {
                categoryMatch = true;
                this.setState({
                    isAddCategoryMatch: true
                })
                return;
            }
        })

        if (!categoryMatch) {
            this.setState({
                isAddCategoryMatch: false
            })
            // バリデーション
            const element = document.getElementById("categoryLen")
            if (value.length > 0 && value.length <= 10) {
                element.style.color = "green"
                this.setState({ isAddCategoryValid: true })
            } else {
                element.style.color = "red"
                this.setState({ isAddCategoryValid: false })
            }
        } else {
            this.setState({
                isAddCategoryMatch: true,
                isAddCategoryValid: false
            })
        }

    }
    handleEditNameChange(event) {
        const value = event.target.value;
        this.setState({
            category: {
                name: value,
                id: this.state.category.id,
                userId: this.state.category.userId
            },
            editCategoryLen: value.length
        })

        // 重複バリデーション
        let categoryMatch = false
        this.props.categoryArray.map((category) => {
            if (category.name == value) {
                categoryMatch = true;
                this.setState({
                    isEditCategoryMatch: true
                })
                return;
            }
        })
        if (!categoryMatch) {
            this.setState({
                isEditCategoryMatch: false
            })

            // バリデーション
            const element = document.getElementById("editCategoryLen");
            if (value.length > 0 && value.length <= 10) {
                element.style.color = "green";
                this.setState({
                    isEditCategory: true
                })
            } else {
                element.style.color = "red";
                this.setState({
                    isEditCategory: false
                })
            }
        } else {
            this.setState({
                isEditCategory: false
            })
        }

    }
    addSubmit() {
        if (this.state.isAddCategoryValid) {
            const data = { name: this.state.categoryName, userId: this.props.user.id };
            fetch("http://localhost:8080/expense/category/add", {
                headers: {
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(data)
            }).then((response) => {
                if (response.status === 200) {
                    response.json().then(json => {
                        this.action.updateCategoryList(json)
                        this.setState({
                            categoryName: "",
                            isCategorySave: true

                        })
                    })
                } else {
                    alert("登録に失敗しました")
                }
            })
        } else {
            if (this.state.categoryLen <= 0) {
                alert("入力してください")
            } else {
                alert("登録できる文字数は10文字以内です")
            }
        }
    }

    addCategory() {
        return (
            <div className="addCategory">
                <Form.Group controlId="formBasicCategoryName">
                    {this.state.isCategorySave &&
                        <Alert variant="info text-center" className="save-success-alert">
                            <p>登録しました</p>
                        </Alert>
                    }
                    <Form.Label>追加するカテゴリ名</Form.Label>
                    <div className="addCategoryMatchError">{this.state.isAddCategoryMatch && 'カテゴリが存在します'}</div>
                    <Form.Control onChange={(event) => this.handleNameChange(event)} value={this.state.categoryName} type="text" />
                    <div className="category-len"><span id="categoryLen">{this.state.categoryLen}</span>/10</div>
                    <Button onClick={() => this.addSubmit()} className="mt-3 mb-3 addButton" disabled={this.state.isAddCategoryValid ? false : true}>＋追加</Button>
                </Form.Group>

            </div>
        )
    }

    editSubmit() {
        if (this.state.isEditCategory) {
            const data = { id: this.state.category.id, name: this.state.category.name, userId: this.props.user.id }
            fetch("http://localhost:8080/expense/category/edit/" + this.props.user.id, {
                headers: {
                    'Content-Type': 'application/json'
                },
                method: "PUT",
                body: JSON.stringify(data)
            }).then((response) => {
                if (response.status === 200) {
                    response.json().then(json => {
                        this.action.updateCategoryList(json)
                        this.handleBoxChange()
                    })
                } else {
                    alert("変更できませんでした")
                }
            })
        } else {
            if (this.state.editCategoryLen >= 10) {
                alert("カテゴリは10文字以内で入力してください")
            } else {
                alert("入力してください")
            }
        }
    }

    selectCtegory(category) {
        this.setState({
            isSelectCategory: true,
            category: category,
            editCategoryLen: category.name.length,
            currentCategory: category.name
        })
    }

    handleBoxChange() {
        this.setState({
            isSelectCategory: false,
            category: []
        })
    }

    currentCategory() {
        return (
            <div className="category-box" >
                <div className="box-header"><div onClick={() => this.handleBoxChange()} className="box-close-button">x</div></div>
                <div className="box-main">
                    <Form.Group controlId="formBasicEditCategoryName">
                        <div className="editMatchError">{this.state.isEditCategoryMatch && 'カテゴリが存在します'}</div>
                        <Form.Control onChange={(event) => this.handleEditNameChange(event)} value={this.state.category.name} type="text" />
                        <div className="edit-category-len"><span id="editCategoryLen">{this.state.editCategoryLen}</span>/10</div>
                        <div className="edit-button-area">
                            <Button id="editButton" className="mx-3" onClick={() => this.editSubmit()} disabled={this.state.isEditCategory ? false : true}>編集</Button>
                            <Button className="btn btn-danger" onClick={() => this.dropCategory(this.state.category.id)}>削除</Button>
                        </div>
                    </Form.Group>
                </div>
            </div>
        )
    }

    categoryIndex() {
        const categories = this.props.categoryArray.map((category) => {
            return (
                <p onClick={() => this.selectCtegory(category)} id={"categoryPiece" + category.id} className="category-piece" key={category.id}>{category.name}</p>
            )
        })
        return (
            <>
                <div className="categories">
                    {this.state.isSelectCategory ?
                        this.currentCategory()
                        :
                        categories
                    }
                </div>
            </>
        )
    }



    render() {
        return (
            <div className="category-main">
                <Tabs>
                    <TabList >
                        <Tab>カテゴリ一覧</Tab>
                        <Tab>カテゴリの追加</Tab>
                    </TabList>
                    <TabPanel>
                        {this.categoryIndex()}
                    </TabPanel>
                    <TabPanel>
                        {this.addCategory()}
                    </TabPanel>
                </Tabs>
            </div>
        )
    }


}

CategoryEdit.propTypes = {
    dispatch: PropTypes.func,
    isSignedIn: PropTypes.bool,
    user: PropTypes.any,
    cookies: PropTypes.any,
    categoryArray: PropTypes.array,
    start: PropTypes.any,
    end: PropTypes.any
}

function mapStateToProps(state) {
    return state
}

export default withCookies(connect(mapStateToProps)(CategoryEdit));