import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import { Col, Form, InputGroup, Button } from 'react-bootstrap';
import moment from 'moment';
import '../../css/form.css';

class AddExpense extends Component {
  constructor(props) {
    super(props)
    this.state = {
      categoryId: 0,
      description: '',
      descriptionLength: 0,
      isDescriptionError: true,
      descriptionErrMsg:"",
      price: "",
      priceLength: 0,
      isPriceError: true,
      priceErrMsg: "",
      buyDate: moment().format('yyyy-MM-DD')
    }
  }

  componentDidMount() {
    fetch("http://localhost:8080/expense/category/" + this.props.user.id, {
      method: "GET"
    })
      .then((response) => {
        response.json().
          then(json => {
            this.setState({categoryId: json[0].id})
          })
      });

      document.getElementById('ExpenseAdd_tab').style.backgroundColor = 'white';

      document.getElementById('MonthlyList_tab').style.backgroundColor = '#CCC';
      document.getElementById('IncomeAdd_tab').style.backgroundColor = '#CCC';
  }

  handleDescriptionChanged(e) {
    const value = e.target.value;
    this.setState({
      description: value,
      descriptionLength: value.length
    });
    // 文字数カウント
    const element = document.getElementById('descriptionLen');
    if (value.length < 1 || value.length > 280) {
      element.style.color = "red";
    } else {
      element.style.color = "green";
    }

    //バリデーション
    const formElement = document.getElementById('formDescription');
    if (value.length < 1 || value.length > 280) {
      this.setState({
          isDescriptionError: true
      })
    } else {
      this.setState({
          isDescriptionError: false
      })
      formElement.style.border = "";
    }
  }

  handlePriceChanged(e) {
    const value = e.target.value;
    this.setState({
      price: value,
      priceLength: value.length
    });
    // 文字数カウント
    const element = document.getElementById('priceLen');
    if (value.length < 1 || value.length > 8) {
      element.style.color = "red";
    } else {
      element.style.color = "green";
    }

    //バリデーション
    const formElement = document.getElementById('formPrice');
    if (value.length < 1 || value.length > 8) {
      this.setState({
          isPriceError: true
      })
    } else if (value.match("[^0-9]")) {
        this.setState({
            isPriceError: true,
            priceErrMsg: "数字のみ使えます"
        })
    } else {
      this.setState({
          isPriceError: false
      })
      formElement.style.border = "";
    }
  }

  sendButtonClicked() {
    if (this.state.isDescriptionError || this.state.isPriceError) {
        if (this.state.description.length == 0) {
          const formElement = document.getElementById("formDescription");
          this.setState({ descriptionErrMsg: "入力してください" })
          formElement.style.border = "2px solid red";
        }
        if (this.state.price.length == 0) {
          const formElement = document.getElementById("formPrice");
          this.setState({ priceErrMsg: "入力してください" })
          formElement.style.border = "2px solid red";
        }
     } else {
        const data = {
          "userId": this.props.user.id,
          "categoryId": this.state.categoryId,
          "description":this.state.description,
          "price": this.state.price,
          "buyDate": this.state.buyDate
        }

        // console.log(data);

      fetch("http://localhost:8080/expense/add", {
        headers: {
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify(data)
      })
      .then((response) => {
        if(response.status === 200) {
          const {cookies} = this.props;
          cookies.set("page", "MonthlyList", {path:"/"});

        } else {
          alert("送信に失敗しました。ステータス")
        }
      })
      .catch(error => console.error(error));
    }
  }

  render() {
    const categorylist = this.props.categoryArray.map((category) => (
      <option value={category.id} key={category.id}>{category.name}</option>
    ));

    const handleChaged = (e) => {
      this.setState({[e.target.name]: e.target.value})
    }

    return(
      <Col md="3" className="add-expense">
        <Form className="form">
          <div className="title-flame expense-flame">
              <p className="title">支出登録</p>
          </div>

          <Form.Group controlId="formCategory">
            <Form.Label>カテゴリー</Form.Label>
            <Form.Control size="sm" as="select" name="categoryId" value={this.state.categoryId} onChange={handleChaged}>
              {categorylist}
            </Form.Control>
          </Form.Group>

          <Form.Group>
            <Form.Label htmlFor="formDescription">メモ</Form.Label>
              <p className="err-msg">
                  {this.state.isDescriptionError && this.state.descriptionErrMsg}
              </p>
            <Form.Control size="sm" as="textarea" rows={3} name="description" id="formDescription" onChange={(e) => this.handleDescriptionChanged(e)} />
            <p className="length-check"><span className="description-length" id="descriptionLen">{this.state.descriptionLength}</span>/280</p>
          </Form.Group>

          <Form.Group controlId="formPrice">
            <Form.Label>金額</Form.Label>
            <p className="err-msg">
                {this.state.isPriceError && this.state.priceErrMsg}
            </p>
            <InputGroup className="inputPrice">
              <Form.Control size="sm" type="text" name="price" aria-describedby="yen" onChange={(e) => this.handlePriceChanged(e)} />
              <InputGroup.Append>
                    <InputGroup.Text id="yen">円</InputGroup.Text>
              </InputGroup.Append>
            </InputGroup>
            <p className="length-check"><span className="price-length" id="priceLen">{this.state.priceLength}</span>/8</p>
          </Form.Group>

          <Form.Group controlId="formBuyDate">
          <Form.Label>日にち</Form.Label>
          <Form.Control size="sm" type="Date" name="buyDate" value={this.state.buyDate} onChange={handleChaged} />
          </Form.Group>

          <div className="flame">
            <Button variant="light" className="submit-button" onClick={() => this.sendButtonClicked()}>追加</Button>
          </div>
        </Form>
      </Col>
    )
  }
}

AddExpense.propTypes = {
  cookies: PropTypes.any,
  user: PropTypes.any,
  categoryArray: PropTypes.array
}

function mapStateToProps(state) {
    return state;
}

export default withCookies(connect(mapStateToProps)(AddExpense));
