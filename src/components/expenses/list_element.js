import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import {bindActionCreators} from 'redux'
import * as actions from '../../action/actions';

import DeleteExpense from './delete';

class ExpenseListElement extends Component {
  constructor(props) {
    super(props)
    const {dispatch} = props;
    this.action = bindActionCreators(actions, dispatch);
  }

  render() {
    const editButtonClicked = () => {
      this.action.editExpense(this.props.expenseId)
      const {cookies} = this.props;
      cookies.set("page", "ExpenseEdit", {path:"/"});
    }

    return(
      <table className="inner-list">
        <div className="list-left">
          <div className="category">【{this.props.category}】</div>
          <div className="description">{this.props.description}</div>
        </div>
        <div className="list-right">
          <span className="">{this.props.price}円</span>
        <div>
          <span className=""><DeleteExpense expenseId={this.props.expenseId}/></span>
          <span className=""><button className="editButton" onClick={editButtonClicked}>編集</button></span>
        </div>
        </div>
      </table>
    )
  }
}

ExpenseListElement.propTypes = {
  dispatch: PropTypes.func,
  cookies: PropTypes.any,
  category: PropTypes.string,
  description: PropTypes.string,
  price: PropTypes.number,
  buyDate: PropTypes.any,
  expenseId: PropTypes.number
}

function mapStateToProps(state) {
    return state
}
export default withCookies(connect(mapStateToProps)(ExpenseListElement));
