import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import {bindActionCreators} from 'redux'
import * as actions from '../../action/actions';

import ExpenseListElement from './list_element.js';

class ExpenseList extends Component {
  constructor(props) {
    super(props)
    const {dispatch} = props;
    this.action = bindActionCreators(actions, dispatch);
  }

  componentDidMount() {
    fetch("http://localhost:8080/expense/getall/" + this.props.user.id, {
      method: "GET"
    })
    .then((response) => {
      response.json().
      then(json => {
      this.action.updateExpenseList(json)
      })
    })
  }

  render() {
    const expenselist = this.props.expenseArray.map((expense) => (
      <ExpenseListElement
        id={expense.id}
        category={expense.category.name}
        description={expense.description}
        price={expense.price}
        buyDate={expense.buyDate}
        key={expense.id}
      />
    ));

    return(
      <div>
        {expenselist}
      </div>
    )
  }
}

ExpenseList.propTypes = {
  dispatch: PropTypes.func,
  expenseArray: PropTypes.array,
  user: PropTypes.any
}

function mapStateToProps(state) {
    return state
}
export default withCookies(connect(mapStateToProps)(ExpenseList));
