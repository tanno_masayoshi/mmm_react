import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import {bindActionCreators} from 'redux'
import * as actions from '../../action/actions';

class DeleteExpense extends Component {
  constructor(props) {
    super(props)
    const {dispatch} = props;
    this.action = bindActionCreators(actions, dispatch);
  }

  render() {
    const deleteButtonClicked = () => {
      confirm("削除してもよろしいですか？") &&
      deleteSend()
    }

    const deleteSend = () => {
      fetch("http://localhost:8080/expense/delete/" + this.props.expenseId, {
        method: "DELETE",
      })
      .then((response) => {
        if(response.status === 200) {
          alert("削除しました")

          fetch("http://localhost:8080/expense/getall/" + this.props.user.id + "/" + this.props.termStart + "/" +  this.props.termEnd, {
            method: "GET"
          })
          .then((response) => {
            response.json().
            then(json => {
            this.action.updateExpenseList(json)
            })
          })

        } else {
          alert("送信に失敗しました。ステータス")
        }
      })
      .catch(error => console.error(error));
    }

    return(
      <button className="deleteButton" onClick={deleteButtonClicked}>削除</button>
    )
  }
}

DeleteExpense.propTypes = {
  dispatch: PropTypes.func,
  expenseId: PropTypes.number,
  user: PropTypes.any,
  termStart: PropTypes.any,
  termEnd: PropTypes.any
}

function mapStateToProps(state) {
    return state;
}

export default withCookies(connect(mapStateToProps)(DeleteExpense));
