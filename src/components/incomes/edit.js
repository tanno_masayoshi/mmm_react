import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import PropTypes from 'prop-types';

import { Col, Form, InputGroup, Button } from 'react-bootstrap';
import moment from 'moment';
import '../../css/form.css';

class EditIncome extends Component {
    constructor(props) {
        super(props)

        this.state = {

          amount: "",
          amountLength: 0,
          isAmountError: false,
          amountErrMsg: "",
          incomeDate: "",
          category: '',
          categoryLength: 0,
          isCategoryError: false,
          categoryErrMsg: ""
        }

    }

    componentDidMount() {

      fetch("http://localhost:8080/incomes/get/" + this.props.editIncomeId, {
        method: "GET"
      })
      .then((response) => {
        response.json().
        then(json => {
          if (json != null) {
            this.setState({
              amount: json.amount,
              category: json.category,
              incomeDate: json.incomeDate
            })
          } else {
            const {cookies} = this.props;
            cookies.set("page", "list", {path:"/"});
          }
        })
      });

      document.getElementById('MonthlyList_tab').style.backgroundColor = 'white';

      document.getElementById('ExpenseAdd_tab').style.backgroundColor = '#CCC';
      document.getElementById('IncomeAdd_tab').style.backgroundColor = '#CCC';
    }


    handleCategoryChanged(e) {
      const value = e.target.value;
      this.setState({
        category: value,
        categoryLength: value.length
      });
      // 文字数カウント
      const element = document.getElementById('categoryLen');
      if (value.length < 1 || value.length > 280) {
        element.style.color = "red";
      } else {
        element.style.color = "green";
      }

      //バリデーション
      const formElement = document.getElementById('formCategory');
      if (value.length < 1 || value.length > 280) {
        this.setState({
            isCategoryError: true
        })
      } else {
        this.setState({
            isCategoryError: false
        })
        formElement.style.border = "";
      }
    }


    handleAmountChanged(e) {
      const value = e.target.value;
      this.setState({
        amount: value,
        amountLength: value.length
      });
      // 文字数カウント
      const element = document.getElementById('amountLen');
      if (value.length < 1 || value.length > 8) {
        element.style.color = "red";
      } else {
        element.style.color = "green";
      }

      //バリデーション
      const formElement = document.getElementById('formAmount');
      if (value.length < 1 || value.length > 8) {
        this.setState({
            isAmountError: true
        })
      } else if (value.match("[^0-9]")) {
          this.setState({
              isAmountError: true,
              amountErrMsg: "数字のみ使えます"
          })
          formElement.style.border = "2px solid red";
      } else {
        this.setState({
            isAmountError: false
        })
        formElement.style.border = "";
      }
    }


    sendButtonClicked() {
      if (this.state.isCategoryError || this.state.isAmountError) {
          if (this.state.category.length == 0) {
            const formElement = document.getElementById("formCategory");
            this.setState({ categoryErrMsg: "入力してください" })
            formElement.style.border = "2px solid red";
          }
          if (this.state.amount.length == 0) {
            const formElement = document.getElementById("formAmount");
            this.setState({ amountErrMsg: "入力してください" })
            formElement.style.border = "2px solid red";
          }
       } else {
         const data = {
           "id": this.props.editIncomeId,
           "amount": this.state.amount,
           "incomeDate": this.state.incomeDate,
           "category": this.state.category,
           "userId": this.props.user.id
         }

      //  console.log(data);

        fetch("http://localhost:8080/incomes/update", {

          headers: {
            'Content-Type': 'application/json'
          },
          method: "POST",
          body: JSON.stringify(data)
        })

        .then((response) => {
          if(response.status === 200) {
            const {cookies} = this.props;
            cookies.set("page", "MonthlyList", {path:"/"});

          } else {
            alert("送信に失敗しました。ステータス")
          }
        })
        .catch(error => console.error(error));
      }
    }

    render() {

      const handleIncomeDateChanged = (e) => {
          this.setState({ incomeDate: e.target.value })
      }

        return (

          <Col md="3" className="edit-income">
            <Form className="form">
              <div className="title-flame income-flame">
                  <p className="title">収入編集</p>
              </div>

              <Form.Group controlId="formCategory">
                <Form.Label>カテゴリー</Form.Label>
                <p className="err-msg">
                    {this.state.isCategoryError && this.state.categoryErrMsg}
                </p>
                <Form.Control size="sm" as="textarea" rows={1} name="category" value={this.state.category} onChange={(e) => this.handleCategoryChanged(e)} />
                <p className="length-check"><span className="category-length" id="categoryLen">{this.state.categoryLength}</span>/280</p>
              </Form.Group>

              <Form.Group controlId="formAmount">
                <Form.Label>収入額</Form.Label>
                <p className="err-msg">
                    {this.state.isAmountError && this.state.amountErrMsg}
                </p>
                <InputGroup className="inputAmount">
                  <Form.Control size="sm" type="text" name="amount" value={this.state.amount} aria-describedby="yen" onChange={(e) => this.handleAmountChanged(e)} />
                  <InputGroup.Append>
                        <InputGroup.Text id="yen">円</InputGroup.Text>
                  </InputGroup.Append>
                </InputGroup>
                <p className="length-check"><span className="amount-length" id="amountLen">{this.state.amountLength}</span>/8</p>
              </Form.Group>

              <Form.Group controlId="formIncomeDate">
                <Form.Label>収入日</Form.Label>
                <Form.Control size="sm" type="Date" name="incomeDate" value={moment(this.state.incomeDate).format("yyyy-MM-DD")} onChange={handleIncomeDateChanged} />
              </Form.Group>

              <div className="flame">
                <Button variant="light" className="submit-button" onClick={() => this.sendButtonClicked()}>変更</Button>
              </div>
            </Form>
          </Col>        )
    }
}

EditIncome.propTypes = {
    cookies: PropTypes.any,
    dispatch: PropTypes.func,

    user: PropTypes.any,

    editIncomeId: PropTypes.number
}

function mapStateToProps(state) {
    return state
}

export default withCookies(connect(mapStateToProps)(EditIncome));
