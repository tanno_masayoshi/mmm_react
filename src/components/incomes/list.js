import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import * as actions from '../../action/actions'
import IncomeListElement from './list_element'

class IncomeList extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch)

    }

    componentDidMount() {
        const userId = this.props.user.id
        return fetch("http://localhost:8080/incomes/getall/" + userId)
            .then((response) => {
                if (response.status === 200) {
                    response.json()
                        .then(json => {
                            this.action.getIncomes(json);
                        })
                } else {
                    alert("収入の取得に失敗しました")
                }
            }).catch(error => console.log(error));
    }


    render() {            
        const incomelist = this.props.incomes.map((income) => (
            <IncomeListElement
                id={income.id}
                incomeDate={income.incomeDate}
                amount={income.amount}
                category={income.category}
                key={income.id}
            />
        ));    
          return (
            <div>
                {incomelist}
            </div>
        )
    }
}

IncomeList.propTypes = {
    dispatch: PropTypes.func,
    incomes: PropTypes.any,
    user: PropTypes.any,
    incomeId: PropTypes.number
}

function mapStateToProps(state) {
    return state
}

export default connect(mapStateToProps)(IncomeList);