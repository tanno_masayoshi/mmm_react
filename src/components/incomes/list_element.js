import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import { bindActionCreators } from 'redux'
import * as actions from '../../action/actions';
import DeleteIncome from './delete';

class IncomeListElement extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch);
    }

    editButtonClicked() {
        this.action.editIncome(this.props.incomeId)
        const { cookies } = this.props;
        cookies.set("page", "IncomeEdit", { path: "/" });
    }
    render() {
        return (
              <table className="inner-list">
                <div className="list-left">
                  <div className="category">【{this.props.category}】</div>
                  <div className="">{this.props.amount}円</div>
                </div>
                <div className="list-right">
                <div>
                  <span className=""><DeleteIncome incomesId={this.props.incomeId} /></span>
                  <span className=""><button className="editButton" onClick={() => this.editButtonClicked()}>編集</button></span>
                </div>
                </div>
              </table>
        );
    }
}

IncomeListElement.propTypes = {
    dispatch: PropTypes.func,
    cookies: PropTypes.any,
    category: PropTypes.string,
    amount: PropTypes.number,
    incomeDate: PropTypes.any,
    incomeId: PropTypes.number
}

function mapStateToProps(state) {
    return state
}
export default withCookies(connect(mapStateToProps)(IncomeListElement));
