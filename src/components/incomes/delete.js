import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import * as actions from '../../action/actions'

class DeleteIncome extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch)
    }

    deleteButtonClicked(incomesId) {
        confirm("選択した列を削除します。よろしいですか？") &&
            this.deleteIncome(incomesId)
    }
    deleteIncome(incomesId) {
        const userId = this.props.user.id;
        fetch("http://localhost:8080/incomes/delete/id=" + incomesId + "&user=" + userId, {
            method: 'DELETE',
        })
            .then((response) => {
                if (response.status === 200) {
                    response.json().then(json => {
                        this.action.getIncomes(json);
                    })
                    alert("削除しました。");
                } else {
                    alert("削除に失敗しました。")
                }
            })
            .catch(error => console.error(error));
    }

    render() {
        return (
            <button className="deleteButton" onClick={() => this.deleteButtonClicked(this.props.incomesId)}>削除</button>
        );
    }
}


DeleteIncome.propTypes = {
    dispatch: PropTypes.func,
    incomes: PropTypes.any,
    user: PropTypes.any,

    incomesId: PropTypes.number
}

function mapStateToProps(state) {
    return state
}

export default connect(mapStateToProps)(DeleteIncome);