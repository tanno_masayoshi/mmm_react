import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../action/actions'
import { withCookies } from 'react-cookie';
import { Col, Form, InputGroup, Button } from 'react-bootstrap';
import moment from 'moment';
import '../../css/form.css'

class AddIncome extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch)
        this.state = {
            amount: "",
            amountLength: 0,
            isAmountError: true,
            amountErrMsg: "",
            incomeDate: moment().format('yyyy-MM-DD'),
            category: '',
            categoryLength: 0,
            isCategoryError: true,
            categoryErrMsg: ""
        }
    }

    componentDidMount() {
      document.getElementById('IncomeAdd_tab').style.backgroundColor = 'white';
      document.getElementById('MonthlyList_tab').style.backgroundColor = '#CCC';
      document.getElementById('ExpenseAdd_tab').style.backgroundColor = '#CCC';
    }

    handleChaged(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleCategoryChanged(e) {
      const value = e.target.value;
      this.setState({
        category: value,
        categoryLength: value.length
      });
      // 文字数カウント
      const element = document.getElementById('categoryLen');
      if (value.length < 1 || value.length > 280) {
        element.style.color = "red";
      } else {
        element.style.color = "green";
      }

      //バリデーション
      const formElement = document.getElementById('formCategory');
      if (value.length < 1 || value.length > 280) {
        this.setState({
            isCategoryError: true
        })
      } else {
        this.setState({
            isCategoryError: false
        })
        formElement.style.border = "";
      }
    }

    handleAmountChanged(e) {
      const value = e.target.value;
      this.setState({
        amount: value,
        amountLength: value.length
      });
      // 文字数カウント
      const element = document.getElementById('amountLen');
      if (value.length < 1 || value.length > 8) {
        element.style.color = "red";
      } else {
        element.style.color = "green";
      }

      //バリデーション
      const formElement = document.getElementById('formAmount');
      if (value.length < 1 || value.length > 8) {
        this.setState({
            isPriceError: true
        })
      } else if (value.match("[^0-9]")) {
          this.setState({
              isAmountError: true,
              amountErrMsg: "数字のみ使えます"
          })
          formElement.style.border = "2px solid red";
      } else {
        this.setState({
            isAmountError: false
        })
        formElement.style.border = "";
      }
    }

    sendButtonClicked() {
      if (this.state.isCategoryError || this.state.isAmountError) {
          if (this.state.category.length == 0) {
            const formElement = document.getElementById("formCategory");
            this.setState({ categoryErrMsg: "入力してください" })
            formElement.style.border = "2px solid red";
          }
          if (this.state.amount.length == 0) {
            const formElement = document.getElementById("formAmount");
            this.setState({ amountErrMsg: "入力してください" })
            formElement.style.border = "2px solid red";
          }
       } else {
         const data = {
           "amount": this.state.amount,
           "income_date": this.state.incomeDate,
           "category": this.state.category,
           "user_id": this.props.user.id
       }

        fetch("http://localhost:8080/incomes/add", {
          headers: {
            'Content-Type': 'application/json'
          },
          method: "POST",
          body: JSON.stringify(data)
        })
        .then((response) => {
            if (response.status === 200) {
                response.json().then(json => {
                    this.action.getIncomes(json);
                });
                const { cookies } = this.props;
                cookies.set("page", "MonthlyList", { path: "/" });
            } else {
                alert("送信に失敗しました。")
            }
        })
        .catch(error => console.error(error));
      }

    }

    render() {
        return (
          <Col md="3" className="add-income">
            <Form className="form">
              <div className="title-flame income-flame">
                  <p className="title">収入登録</p>
              </div>

              <Form.Group controlId="formCategory">
                <Form.Label>カテゴリー</Form.Label>
                <p className="err-msg">
                    {this.state.isCategoryError && this.state.categoryErrMsg}
                </p>
                <Form.Control size="sm" as="textarea" rows={1} name="category" onChange={(e) =>this.handleCategoryChanged(e)} />
                <p className="length-check"><span className="category-length" id="categoryLen">{this.state.categoryLength}</span>/280</p>
              </Form.Group>

              <Form.Group controlId="formAmount">
                <Form.Label>収入額</Form.Label>
                <p className="err-msg">
                    {this.state.isAmountError && this.state.amountErrMsg}
                </p>
                <InputGroup className="inputAmount">
                  <Form.Control size="sm" type="text" name="amount" aria-describedby="yen" onChange={(e) =>this.handleAmountChanged(e)} />
                  <InputGroup.Append>
                        <InputGroup.Text id="yen">円</InputGroup.Text>
                  </InputGroup.Append>
                </InputGroup>
                <p className="length-check"><span className="amount-length" id="amountLen">{this.state.amountLength}</span>/8</p>
              </Form.Group>

              <Form.Group controlId="formIncomeDate">
              <Form.Label>収入日</Form.Label>
              <Form.Control size="sm"type="Date" name="incomeDate" value={this.state.incomeDate} onChange={(e) => this.handleChaged(e)} />
              </Form.Group>

              <div className="flame">
                <Button variant="light" className="submit-button" onClick={() => this.sendButtonClicked()}>追加</Button>
              </div>
            </Form>
          </Col>
        )
    }
}


AddIncome.propTypes = {
    user: PropTypes.any,
    cookies: PropTypes.any,
    dispatch: PropTypes.func,
}

function mapStateToProps(state) {
    return state;
}

export default withCookies(connect(mapStateToProps)(AddIncome));
